<?php

/**
 * @file
 * Supports inline conditions and, thus, Commerce Discounts.
 */

/**
 * Implements hook_inline_conditions_info().
 */
function commerce_order_total_inline_conditions_info() {
  $conditions = array();

  $conditions['commerce_order_total_condition'] = array(
    'label' => t('Total amount (advanced)'),
    'entity type' => 'commerce_order',
    'callbacks' => array(
      'configure' => 'commerce_order_total_condition_configure',
      'build' => 'commerce_order_total_condition',
    ),
  );

  return $conditions;
}

/**
 * Configuration callback for commerce_order_total_condition.
 *
 * @param array $settings
 *   An array of rules condition settings.
 *
 * @return array;
 *   A form element array.
 */
function commerce_order_total_condition_configure($settings) {
  $form = array();

  // Ensure we have default values for the condition settings.
  $settings += array(
    'operator' => '>=',
    'value' => 0,
    'exclude_types' => [],
    'exclude_prod_types' => [],
    'exclude_shipping_services' => [],
    'skus' => "",
  );

  $form['operator'] = array(
    '#type' => 'select',
    '#title' => t('Operator'),
    '#options' => commerce_numeric_comparison_operator_options_list(),
    '#require' => TRUE,
    '#default_value' => $settings['operator'],
    '#prefix' => '<div style="clear:both">',
    '#suffix' => '<br>The comparison operator to use against the order total.</div>',
  );

  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => "<br />" . t('Value'),
    '#size' => 10,
    '#default_value' => $settings['value'],
    '#prefix' => '<div style="clear:both">',
    '#suffix' => '<br>Integer representing a value in minor currency units to compare against, such as 1000 for $10.</div>',
  );

  $form['exclude_types'] = array(
    '#type' => 'checkboxes',
    '#title' => "<br />" . t('Exclude these Line Item Types') . "<br />",
    '#options' => commerce_order_total_line_item_types(),
    '#default_value' => $settings['exclude_types'],
    '#prefix' => '<div style="clear:both">',
    '#suffix' => '</div>',
  );

  $form['exclude_prod_types'] = array(
    '#type' => 'checkboxes',
    '#title' => "<br />" . t('Exclude these Product Types') . "<br />",
    '#options' => commerce_order_total_product_types(),
    '#default_value' => $settings['exclude_prod_types'],
    '#prefix' => '<div style="clear:both">',
    '#suffix' => '</div>',
  );

  $form['exclude_shipping_services'] = array(
    '#type' => 'checkboxes',
    '#title' => '<br>' . t('Exclude these Shipping Services'),
    '#options' => commerce_order_total_shipping_services(),
    '#default_value' => $settings['exclude_shipping_services'],
    '#prefix' => '<div style="clear:both">',
    '#suffix' => t('<br>Note that shipping services are often not available in the order object unless we are at the end of the checkout process.</div>'),
  );

  $form['skus'] = array(
    '#type' => 'textfield',
    '#maxlength' => 4096,
    '#default_value' => $settings['skus'],
    '#title' => "<br />" . t('Exclude these SKUs') . "<br />",
    '#suffix' => t('<br>Separate each sku with a comma.'),
  );

  return $form;
}
